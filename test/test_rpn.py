import unittest

from src.rpn import RPN, Pica


# ----------------
# Basic Pica tests
# ----------------

class TestPica(unittest.TestCase):
    def test_pica_creation_from_int(self):
        self.assertEqual(Pica(6).points, 6)
        self.assertEqual(Pica(12).points, 12)
        self.assertEqual(Pica(18).points, 18)

    def test_pica_creation_from_float(self):
        self.assertEqual(Pica(3.1415).points, 3.1415)
        self.assertEqual(Pica(12.25).points, 12.25)
        self.assertEqual(Pica(18.5).points, 18.5)

    def test_pica_creation_from_string(self):
        self.assertEqual(Pica('0p6').points, 6)
        self.assertEqual(Pica('0p6.23').points, 6.23)
        self.assertEqual(Pica('1p0').points, 12)
        self.assertEqual(Pica('1p0.8475').points, 12.8475)
        self.assertEqual(Pica('1p6').points, 18)
        self.assertEqual(Pica('1p6.5').points, 18.5)


# --------------
# Addition tests
# --------------

class TestAddition(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_simple_integer_addition(self):
        self.assertEqual(self.calc.rpn([3, 4, '+']), [7])

    def test_simple_float_addition(self):
        self.assertEqual(self.calc.rpn([2.0, 3.0, '+']), [5.0])
        self.assertAlmostEqual(self.calc.rpn([128.23, 0.2, '+']).pop(), 128.43)

    def test_complex_integer_addition(self):
        self.assertEqual(self.calc.rpn([3, 4, 5, '+', '+']), [12])

    def test_complex_float_addition(self):
        self.assertEqual(self.calc.rpn([3.1, 4.2, 5.1, '+', '+']), [12.4])

    def test_mixed_integer_float_addition(self):
        self.assertAlmostEqual(self.calc.rpn([3.1415, 2, '+']).pop(), 5.1415)
        self.assertAlmostEqual(self.calc.rpn([3.1415, 3.120485, '+']).pop(), 6.261985)


# -----------------
# Subtraction tests
# -----------------

class TestSubtraction(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_simple_integer_subtraction(self):
        self.assertEqual(self.calc.rpn([10, 4, '-']), [6])

    def test_complex_integer_subtraction(self):
        self.assertEqual(self.calc.rpn([8, 10, 5, '-', '-']), [3])


# --------------------
# Multiplication tests
# --------------------

class TestMultiplication(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_simple_integer_multiplication(self):
        self.assertEqual(self.calc.rpn([3, 4, '*']), [12])

    def test_complex_integer_multiplication(self):
        self.assertEqual(self.calc.rpn([3, 4, 5, '*', '*']), [60])


# --------------
# Division tests
# --------------

class TestDivision(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_simple_integer_division(self):
        self.assertEqual(self.calc.rpn([10, 2, '/']), [5.0])

    def test_complex_integer_division(self):
        self.assertEqual(self.calc.rpn([40, 10, 2, '/', '/']), [8.0])


# --------------
# Combined tests
# --------------

class TestCombinations(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_combination_1(self):
        self.assertEqual(self.calc.rpn([3, 4, 5, '+', '*']), [27])


# ----------------------
# Pica values
# ----------------------

class TestPicaMath(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_single_pica(self):
        self.assertEqual(self.calc.rpn(['10p3']), [Pica('10p3')])

    def test_pica_subtracted_from_pica(self):
        self.assertEqual(self.calc.rpn(['10p3', '2p1', '-']), [Pica('8p2.0')])
        self.assertEqual(self.calc.rpn(['10p3', '2p1.5', '-']), [Pica('8p1.5')])

    def test_pica_added_to_pica(self):
        self.assertEqual(self.calc.rpn(['10p3', '2p2', '+']), [Pica('12p5')])
        self.assertEqual(self.calc.rpn(['10p3', '2p2.5', '+']), [Pica('12p5.5')])

    def test_pica_divided_by_number(self):
        self.assertEqual(self.calc.rpn(['10p3', 2, '/']), [Pica('5p1.5')])
        self.assertEqual(self.calc.rpn(['10p0', 2.5, '/']), [Pica('4p0')])

    def test_pica_multiplied_by_number(self):
        self.assertEqual(self.calc.rpn(['10p3', 3, '*']), [Pica('30p9')])
        self.assertEqual(self.calc.rpn(['10p3', 2.5, '*']), [Pica('25p7.5')])

    def test_number_multiplied_by_pica(self):
        self.assertEqual(self.calc.rpn([3, '10p3', '*']), [Pica('30p9')])
        self.assertEqual(self.calc.rpn([2.5, '10p3', '*']), [Pica('25p7.5')])


# ------------------------------------------------------
# Confirm that we don't support cases that make no sense
# ------------------------------------------------------

class TestNonsenseCases(unittest.TestCase):
    calc = None

    def setUp(self):
        self.calc = RPN()

    def test_cannot_subtract_number_from_pica(self):
        self.assertRaises(TypeError, self.calc.rpn, ['10p3', 2, '-'])
        self.assertRaises(TypeError, self.calc.rpn, ['10p3', 2.5, '-'])

    def test_cannot_subtract_pica_from_number(self):
        self.assertRaises(TypeError, self.calc.rpn, [2, '10p3', '-'])
        self.assertRaises(TypeError, self.calc.rpn, [2.5, '10p3', '-'])

    def test_cannot_add_number_to_pica(self):
        self.assertRaises(TypeError, self.calc.rpn, ['10p3', 2, '+'])
        self.assertRaises(TypeError, self.calc.rpn, ['10p3', 2.5, '+'])

    def test_cannot_add_pica_to_number(self):
        self.assertRaises(TypeError, self.calc.rpn, [2, '10p3', '+'])
        self.assertRaises(TypeError, self.calc.rpn, [2.5, '10p3', '+'])

    def test_cannot_divide_number_by_pica(self):
        self.assertRaises(TypeError, self.calc.rpn, [2, '10p3', '/'])
        self.assertRaises(TypeError, self.calc.rpn, [2.5, '10p3', '/'])

    def test_cannot_multiply_pica_by_pica(self):
        self.assertRaises(TypeError, self.calc.rpn, ['5p6', '10p3', '*'])

    def test_cannot_divide_pica_by_pica(self):
        self.assertRaises(TypeError, self.calc.rpn, ['5p6', '10p3', '/'])


# --------------
# Run unit tests
# --------------

if __name__ == '__main__':
    unittest.main()
