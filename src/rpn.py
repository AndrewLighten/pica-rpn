import re

POINTS_PER_PICA = 12


class Pica(object):
    # initialise
    def __init__(self, points):
        if isinstance(points, str):
            a, b = points.split('p')
            points = int(a) * POINTS_PER_PICA + float(b)
        self.points = points

    # equality test
    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.points == other.points

    # text representation
    def __repr__(self):
        whole_points = int(self.points / POINTS_PER_PICA)
        sub_points = self.points - (whole_points * POINTS_PER_PICA)
        return "'%rp%r'" % (whole_points, sub_points)

    # pica divided by something else
    def __truediv__(self, other):
        if type(other) is int or type(other) is float:
            return Pica(self.points / other)
        raise TypeError

    # something else divided by pica
    def __rtruediv__(self, other):
        raise TypeError

    # pica added to something else
    def __add__(self, other):
        if type(other) is Pica:
            return Pica(self.points + other.points)
        raise TypeError

    # something else added to pica
    def __radd__(self, other):
        raise TypeError

    # pica subtracted from something else
    def __sub__(self, other):
        if type(other) is Pica:
            return Pica(self.points - other.points)
        raise TypeError

    # something else subtracted from pica
    def __rsub__(self, other):
        raise TypeError

    # pica multiplied by something else
    def __mul__(self, other):
        if type(other) is int or type(other) is float:
            return Pica(self.points * other)
        raise TypeError

    # something else multiplied by pica
    def __rmul__(self, other):
        if type(other) is int or type(other) is float:
            return Pica(self.points * other)
        raise TypeError


class RPN:
    def __init__(self):
        self.oper_dict = {'+': RPN.pop_two_and_add,
                          '-': RPN.pop_two_and_sub,
                          '*': RPN.pop_two_and_mul,
                          '/': RPN.pop_two_and_div}
        self.pica_pattern = re.compile("(\d+)p(\d+(\.\d+)?)")

    @staticmethod
    def swap_top_two(terms):
        (v1, v2) = (terms.pop(), terms.pop())
        [terms.append(x) for x in [v1, v2]]

    def pop_two_and_add(self, terms):
        terms.append(terms.pop() + terms.pop())

    def pop_two_and_sub(self, terms):
        self.swap_top_two(terms)
        terms.append(terms.pop() - terms.pop())

    def pop_two_and_mul(self, terms):
        terms.append(terms.pop() * terms.pop())

    def pop_two_and_div(self, terms):
        self.swap_top_two(terms)
        terms.append(terms.pop() / terms.pop())

    def rpn(self, terms):
        result = []
        for term in terms:
            if type(term) is int or type(term) is float:
                result.append(term)
            elif term in self.oper_dict:
                self.oper_dict[term](self, result)
            elif self.pica_pattern.match(term):
                match = self.pica_pattern.match(term)
                result.append(Pica(int(match.group(1)) * POINTS_PER_PICA + float(match.group(2))))
            else:
                raise SyntaxError
        return result
